﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client_train
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" || textBox2.Text != "")
            {
                string message = textBox1.Text + "#" + textBox2.Text + "#";// message = login + password
                Connect("192.168.0.106", message);
            }
            else
            {
                MessageBox.Show("Поля не заповнені");
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2(this);
            f2.Show();

        }
        public void Connect(String server, String message)
        {
            // index = 1  -   надсилання запита для перевірки аунтифікації
            int index = 1;
                try{
                    // створюєм TcpClient.
                    // Для е TcpListener 
                    // настроюєм на IP нашего сервера и той самий порт.
                    String responseData = String.Empty;
                    Int32 port = 58190;
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient(server, port);
                    // Переводимо наше повідомлення в ЮТФ8, а потім в масив Byte.
                    Byte[] data = System.Text.Encoding.UTF8.GetBytes(index.ToString() + message);
                     // Получаем поток для чтения и записи данных.
                    NetworkStream stream = client.GetStream();
                    //відправяєм повідомлення  серверу.
                    stream.Write(data, 0, data.Length);
                    data = new Byte[512];  // Строка для хранения полученных данных.
                    // Читаем перший пакет відповіді сервера. 
                    // можна читати всу повідомнення
                    // дя цього треба зробити цикл як на сервері     

                    int krt = stream.Read(data, 0, data.Length);
                    responseData = System.Text.Encoding.UTF8.GetString(data, 0, krt);

                    String cod = String.Empty;
                    cod += responseData[0];
                    //Convert.ToInt32(cod) == 0 помилка
                    //Convert.ToInt32(cod) == 1 приймання 1 рядка даних
                    //Convert.ToInt32(cod) == 2...n приймання від 2 до n рядків даних
                    if (Convert.ToInt32(cod) != 9)  {
                        if (Convert.ToInt32(cod) == 1){
                            String user_name ="";
                            String user_second_name = "";
                            String user_login = "";
                            String user_password = "";
                            String phone_number = "";
                            String email = "";
                            //розбір вхідного пакета даних на окремі змінні 
                            for (int i = 2, k = 0; i < responseData.Length; i++)
                            {
                                if (responseData[i] == '#') k++;
                                else
                                {
                                    if ((responseData[i] != '#') && (k == 0)) user_name += responseData[i];
                                    if ((responseData[i] != '#') && (k == 1)) user_second_name += responseData[i];
                                    if ((responseData[i] != '#') && (k == 2)) user_login += responseData[i];
                                    if ((responseData[i] != '#') && (k == 3)) user_password += responseData[i];
                                    if ((responseData[i] != '#') && (k == 4)) phone_number += responseData[i];
                                    if ((responseData[i] != '#') && (k == 5)) email += responseData[i];
                                    if ((responseData[i] != '#') && (k == 6))
                                    {
                                         break;
                                    }
                                }
                            }
                           
                            try{
                                String s = textBox1.Text.ToString();
                                Form3 f3 = new Form3(this, s);
                                f3.Show();
                            }
                            catch{
                                MessageBox.Show("Відсутнє з'єднання з інтернетом");
                            }


                        }
                        else {
                            MessageBox.Show("\t Неправильно введено логін чи пароль . ");
                        }
                        stream.Close();
                        client.Close();
                    }
                }
                catch (ArgumentNullException e) {
                    Console.WriteLine("ArgumentNullException: {0}", e);
                    MessageBox.Show("Немає звязку з сервером");
                }
                catch (SocketException e){
                    Console.WriteLine("SocketException: {0}", e);
                    MessageBox.Show("Немає звязку з сервером");
                }
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}
