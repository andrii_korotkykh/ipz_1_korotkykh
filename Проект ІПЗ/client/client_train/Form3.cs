﻿using client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client_train
{
    public partial class Form3 : Form
    {

        public Form3(Form1 f1)
        {
            InitializeComponent();
            //Text += s;
            hide();
        }
        public Form3(Form1 f1, String s)
        {
            InitializeComponent();
            Text += " ( " + s + " )";
            hide();
        }
        void show()
        {
            this.Height = 400;

        }
        void hide()
        {
            this.Height = 260;
        }
        private void button1_Click(object sender, EventArgs e)
        {

            listView1.Columns.Clear();
            listView1.Items.Clear();
            if (textBox1.Text == "")
                MessageBox.Show("Виберіть офіс з випадаючого списку");
            else
            {
                show();
                string message = textBox1.Text + "#" ; //from  + to   
                Connect("192.168.0.106", message, 3);
            }
        }

      

        private void button2_Click(object sender, EventArgs e)
        {
            String s = textBox1.Text;
          
        }

        public void Connect(String server, String message, int index)
        {
            // index = 1  -   надсилання запита для перевірки аунтифікації
            // index = 2  -   надсилання запита для реєстрації користувача
            // index = 3  -   надсилання запита для пошуку маршрута
            // index = 4  -   надсилання запита для пошука місць
            // index = 5  -   надсилання запита для оформлення замовлення

            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////
            //
            //index = 3  -   надсилання запита для пошуку маршрута
            //
            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////
            if (index == 3)
            {
                try
                {
                    // створюєм TcpClient.
                    // Для е TcpListener 
                    // настроюєм на IP нашего сервера и той самий порт.
                    String responseData = String.Empty;

                    Int32 port = 58190;
                    TcpClient client = new TcpClient(server, port);

                    // Переводимо наше повідомлення в ЮТФ8, а потім в масив Byte.
                    Byte[] data = System.Text.Encoding.UTF8.GetBytes(index.ToString() + message);

                    // Получаем поток для чтения и записи данных.
                    NetworkStream stream = client.GetStream();

                    //відправяєм повідомлення  серверу. 

                    stream.Write(data, 0, data.Length);


                    data = new Byte[512];

                    // Строка для хранения полученных данных.

                    // Читаем перший пакет відповіді сервера. 
                    // можна читати всу повідомнення
                    // дя цього треба зробити цикл як на сурвері     

                    int krt = stream.Read(data, 0, data.Length);
                    responseData = System.Text.Encoding.UTF8.GetString(data, 0, krt);

                    String cod = String.Empty;
                    cod += responseData[0];
                    //Convert.ToInt32(cod) == 0 помилка
                    //Convert.ToInt32(cod) == 1 приймання 1 рядка даних
                    //Convert.ToInt32(cod) == 2...n приймання від 2 до n рядків даних
                    if (Convert.ToInt32(cod) != 9)
                    {
                        //створення колонок listView1
                        listView1.Visible = true;
                        listView1.Columns.Add(new ColHeader("Офіс", 110, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("Адреса", 200, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("Начальник", 100, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("Телефон", 100, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("Структура", 50, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("Вільних місць", 70, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("", 60, HorizontalAlignment.Left, true));
                        listView1.Columns.Add(new ColHeader("ID", 80, HorizontalAlignment.Left, true));



                        if (Convert.ToInt32(cod) != 1)
                        {
                            String[] from = new String[Convert.ToInt32(cod)];
                            String[] addr = new String[Convert.ToInt32(cod)];
                            String[] master = new String[Convert.ToInt32(cod)];
                            String[] phone_kom = new String[Convert.ToInt32(cod)];
                            String[] faculty = new String[Convert.ToInt32(cod)];
                            String[] free_t = new String[Convert.ToInt32(cod)];
                            String[] id_gurt = new String[Convert.ToInt32(cod)];

                            //розбір вхідного пакета даних на окремі змінні 
                            for (int i = 2, k = 0, y = 0; i < responseData.Length; i++)
                            {
                                if (responseData[i] == '#') k++;
                                else
                                {
                                    if ((responseData[i] != '#') && (k == 0)) from[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 1)) addr[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 2)) master[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 3)) phone_kom[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 4)) faculty[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 5)) free_t[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 6)) id_gurt[y] += responseData[i];
                                    if ((responseData[i] != '#') && (k == 7))
                                    {
                                        if (y == Convert.ToInt32(cod) - 1) break;
                                        else { y++; k = 0; i--; }
                                    }
                                }
                            }

                            // добавлення рядків з даними в listView1                         
                            for (int i = 0; i < Convert.ToInt32(cod); i++)
                            {
                                add(from[i], addr[i], master[i], phone_kom[i], faculty[i], free_t[i], id_gurt[i], i);
                            }


                            // Connect the ListView.ColumnClick event to the ColumnClick event handler.
                            this.listView1.ColumnClick += new ColumnClickEventHandler(listView1_ColumnClick);

                            // you need to add a listView named listView1 with the designer
                            listView1.FullRowSelect = true;
                            ListViewExtender extender = new ListViewExtender(listView1);
                            // extend 2nd column
                            ListViewButtonColumn buttonAction = new ListViewButtonColumn(6);
                            buttonAction.Click += OnButtonActionClick;
                            buttonAction.FixedWidth = true;

                            extender.AddColumn(buttonAction);

                        }
                        else
                        {
                            String from = String.Empty;
                            String addr = String.Empty;
                            String master = String.Empty;
                            String phone_kom = String.Empty;
                            String faculty = String.Empty;
                            String free_t = String.Empty;
                            String id_gurt = String.Empty;

                            //розбір вхідного пакета даних на окремі змінні 
                            for (int i = 2, k = 0; i < responseData.Length - 1; i++)
                            {
                                if (responseData[i] == '#') k++;
                                else
                                    if ((responseData[i] != '#') && (k == 0)) from += responseData[i];
                                if ((responseData[i] != '#') && (k == 1)) addr += responseData[i];
                                if ((responseData[i] != '#') && (k == 2)) master += responseData[i];
                                if ((responseData[i] != '#') && (k == 3)) phone_kom += responseData[i];
                                if ((responseData[i] != '#') && (k == 4)) faculty += responseData[i];
                                if ((responseData[i] != '#') && (k == 5)) free_t += responseData[i];
                                if ((responseData[i] != '#') && (k == 6)) id_gurt += responseData[i];

                            }
                            // добавлення рядка з даними в listView1
                            add(from, addr, master, phone_kom, faculty, free_t, id_gurt, 0);

                            // Connect the ListView.ColumnClick event to the ColumnClick event handler.
                            this.listView1.ColumnClick += new ColumnClickEventHandler(listView1_ColumnClick);

                            // you need to add a listView named listView1 with the designer
                            listView1.FullRowSelect = true;
                            ListViewExtender extender = new ListViewExtender(listView1);
                            // extend 2nd column
                            ListViewButtonColumn buttonAction = new ListViewButtonColumn(6);
                            buttonAction.Click += OnButtonActionClick;
                            buttonAction.FixedWidth = true;

                            extender.AddColumn(buttonAction);
                        }
                    }
                    else
                    {
                        MessageBox.Show("\tПошук не дав результатів .\n Можливо ви неправильно ввели назви гуртожитку. ");
                    }
                    stream.Close();
                    client.Close();
                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine("ArgumentNullException: {0}", e);
                    MessageBox.Show("Немає звязку з сервером");
                }
                catch (SocketException e)
                {
                    Console.WriteLine("SocketException: {0}", e);
                    MessageBox.Show("Немає звязку з сервером");
                }

            }


        }

        private void OnButtonActionClick(object sender, ListViewColumnMouseEventArgs e)
        {


            hide();
            int l = e.Item.Index;
            String k = listView1.Items[l].SubItems[7].ToString();
            String message = String.Empty;
            // дістаю ID bus 
            for (int i = 0, ki = 0; i < k.Length; i++)
            {
                if (k[i] == '{') ki++;
                else
                {
                    if (k[i] == '}') break;
                    else
                        if (ki == 1) message += k[i];

                }

            }

            Form4 f4 = new Form4(message, this.Text);
            f4.Show();
            /*
            id_bus.Text = message;

            String k2 = listView1.Items[l].SubItems[3].ToString();
            String message2 = String.Empty;
            time_bus.Text = "";
            // дістаю ID bus 
            for (int i = 0, ki = 0; i < k2.Length; i++)
            {
                if (k2[i] == '{') ki++;
                else
                {
                    if (k2[i] == '}') break;
                    else
                        if (ki == 1) message2 += k2[i];

                }

            }
             * */
            Connect("192.168.0.106", message, 4);
            //message = ІД 

        }


        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Create an instance of the ColHeader class.
            ColHeader clickedCol = (ColHeader)this.listView1.Columns[e.Column];

            // Set the ascending property to sort in the opposite order.
            clickedCol.ascending = !clickedCol.ascending;

            // Get the number of items in the list.
            int numItems = this.listView1.Items.Count;

            // Turn off display while data is repoplulated.
            this.listView1.BeginUpdate();

            // Populate an ArrayList with a SortWrapper of each list item.
            System.Collections.ArrayList SortArray = new System.Collections.ArrayList();
            for (int i = 0; i < numItems; i++)
            {
                SortArray.Add(new SortWrapper(this.listView1.Items[i], e.Column));
            }

            // Sort the elements in the ArrayList using a new instance of the SortComparer
            // class. The parameters are the starting index, the length of the range to sort,
            // and the IComparer implementation to use for comparing elements. Note that
            // the IComparer implementation (SortComparer) requires the sort
            // direction for its constructor; true if ascending, othwise false.
            SortArray.Sort(0, SortArray.Count, new SortWrapper.SortComparer(clickedCol.ascending));

            // Clear the list, and repopulate with the sorted items.
            this.listView1.Items.Clear();
            for (int i = 0; i < numItems; i++)
                this.listView1.Items.Add(((SortWrapper)SortArray[i]).sortItem);

            // Turn display back on.
            this.listView1.EndUpdate();
        }
        // The ColHeader class is a ColumnHeader object with an
        // added property for determining an ascending or descending sort.
        // True specifies an ascending order, false specifies a descending order.

        public class SortWrapper
        {
            internal ListViewItem sortItem;
            internal int sortColumn;


            // A SortWrapper requires the item and the index of the clicked column.
            public SortWrapper(ListViewItem Item, int iColumn)
            {
                sortItem = Item;
                sortColumn = iColumn;
            }

            // Text property for getting the text of an item.
            public string Text
            {
                get
                {
                    return sortItem.SubItems[sortColumn].Text;
                }
            }

            // Implementation of the IComparer
            // interface for sorting ArrayList items.
            public class SortComparer : System.Collections.IComparer
            {
                bool ascending;

                // Constructor requires the sort order;
                // true if ascending, otherwise descending.
                public SortComparer(bool asc)
                {
                    this.ascending = asc;
                }

                // Implemnentation of the IComparer:Compare
                // method for comparing two objects.
                public int Compare(object x, object y)
                {
                    SortWrapper xItem = (SortWrapper)x;
                    SortWrapper yItem = (SortWrapper)y;

                    string xText = xItem.sortItem.SubItems[xItem.sortColumn].Text;
                    string yText = yItem.sortItem.SubItems[yItem.sortColumn].Text;
                    return xText.CompareTo(yText) * (this.ascending ? 1 : -1);
                }
            }
        }
        // The ColHeader class is a ColumnHeader object with an
        // added property for determining an ascending or descending sort.
        // True specifies an ascending order, false specifies a descending order.
        public class ColHeader : ColumnHeader
        {
            public bool ascending;
            public ColHeader(string text, int width, HorizontalAlignment align, bool asc)
            {
                this.Text = text;
                this.Width = width;
                this.TextAlign = align;
                this.ascending = asc;
            }
        }
        public void add(string from, string addr, string master, string phone_kom, string faculty, string free_t, string id_gurt, int y)
        {

            listView1.Items.Add(from);
            listView1.Items[y].SubItems.Add(addr);
            listView1.Items[y].SubItems.Add(master);
            listView1.Items[y].SubItems.Add(phone_kom);
            listView1.Items[y].SubItems.Add(faculty);
            listView1.Items[y].SubItems.Add(free_t);

            if (Convert.ToInt32(free_t) != 0)
                listView1.Items[y].SubItems.Add("Відмітитись");
            else listView1.Items[y].SubItems.Add("");
            listView1.Items[y].SubItems.Add(id_gurt);

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }


    }
}
