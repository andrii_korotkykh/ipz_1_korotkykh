﻿namespace client_train
{
    partial class Form4
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.linkLabel59 = new System.Windows.Forms.LinkLabel();
            this.label1 = new System.Windows.Forms.Label();
            this.linkLabel57 = new System.Windows.Forms.LinkLabel();
            this.linkLabel72 = new System.Windows.Forms.LinkLabel();
            this.linkLabel71 = new System.Windows.Forms.LinkLabel();
            this.linkLabel70 = new System.Windows.Forms.LinkLabel();
            this.linkLabel69 = new System.Windows.Forms.LinkLabel();
            this.linkLabel68 = new System.Windows.Forms.LinkLabel();
            this.linkLabel67 = new System.Windows.Forms.LinkLabel();
            this.linkLabel66 = new System.Windows.Forms.LinkLabel();
            this.linkLabel65 = new System.Windows.Forms.LinkLabel();
            this.linkLabel64 = new System.Windows.Forms.LinkLabel();
            this.linkLabel63 = new System.Windows.Forms.LinkLabel();
            this.linkLabel62 = new System.Windows.Forms.LinkLabel();
            this.linkLabel61 = new System.Windows.Forms.LinkLabel();
            this.linkLabel60 = new System.Windows.Forms.LinkLabel();
            this.linkLabel58 = new System.Windows.Forms.LinkLabel();
            this.linkLabel56 = new System.Windows.Forms.LinkLabel();
            this.linkLabel55 = new System.Windows.Forms.LinkLabel();
            this.linkLabel54 = new System.Windows.Forms.LinkLabel();
            this.linkLabel53 = new System.Windows.Forms.LinkLabel();
            this.linkLabel52 = new System.Windows.Forms.LinkLabel();
            this.linkLabel51 = new System.Windows.Forms.LinkLabel();
            this.linkLabel50 = new System.Windows.Forms.LinkLabel();
            this.linkLabel49 = new System.Windows.Forms.LinkLabel();
            this.linkLabel48 = new System.Windows.Forms.LinkLabel();
            this.linkLabel47 = new System.Windows.Forms.LinkLabel();
            this.linkLabel46 = new System.Windows.Forms.LinkLabel();
            this.linkLabel45 = new System.Windows.Forms.LinkLabel();
            this.linkLabel44 = new System.Windows.Forms.LinkLabel();
            this.linkLabel43 = new System.Windows.Forms.LinkLabel();
            this.linkLabel42 = new System.Windows.Forms.LinkLabel();
            this.linkLabel41 = new System.Windows.Forms.LinkLabel();
            this.linkLabel40 = new System.Windows.Forms.LinkLabel();
            this.linkLabel39 = new System.Windows.Forms.LinkLabel();
            this.linkLabel38 = new System.Windows.Forms.LinkLabel();
            this.linkLabel37 = new System.Windows.Forms.LinkLabel();
            this.linkLabel36 = new System.Windows.Forms.LinkLabel();
            this.linkLabel35 = new System.Windows.Forms.LinkLabel();
            this.linkLabel34 = new System.Windows.Forms.LinkLabel();
            this.linkLabel33 = new System.Windows.Forms.LinkLabel();
            this.linkLabel32 = new System.Windows.Forms.LinkLabel();
            this.linkLabel31 = new System.Windows.Forms.LinkLabel();
            this.linkLabel30 = new System.Windows.Forms.LinkLabel();
            this.linkLabel29 = new System.Windows.Forms.LinkLabel();
            this.linkLabel28 = new System.Windows.Forms.LinkLabel();
            this.linkLabel27 = new System.Windows.Forms.LinkLabel();
            this.linkLabel26 = new System.Windows.Forms.LinkLabel();
            this.linkLabel25 = new System.Windows.Forms.LinkLabel();
            this.linkLabel24 = new System.Windows.Forms.LinkLabel();
            this.linkLabel23 = new System.Windows.Forms.LinkLabel();
            this.linkLabel22 = new System.Windows.Forms.LinkLabel();
            this.linkLabel21 = new System.Windows.Forms.LinkLabel();
            this.linkLabel20 = new System.Windows.Forms.LinkLabel();
            this.linkLabel19 = new System.Windows.Forms.LinkLabel();
            this.linkLabel18 = new System.Windows.Forms.LinkLabel();
            this.linkLabel17 = new System.Windows.Forms.LinkLabel();
            this.linkLabel16 = new System.Windows.Forms.LinkLabel();
            this.linkLabel15 = new System.Windows.Forms.LinkLabel();
            this.linkLabel14 = new System.Windows.Forms.LinkLabel();
            this.linkLabel13 = new System.Windows.Forms.LinkLabel();
            this.linkLabel12 = new System.Windows.Forms.LinkLabel();
            this.linkLabel11 = new System.Windows.Forms.LinkLabel();
            this.linkLabel10 = new System.Windows.Forms.LinkLabel();
            this.linkLabel9 = new System.Windows.Forms.LinkLabel();
            this.linkLabel8 = new System.Windows.Forms.LinkLabel();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.linkLabel6 = new System.Windows.Forms.LinkLabel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.button1.Location = new System.Drawing.Point(666, 382);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 40);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ввійти";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(60, 382);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(125, 40);
            this.button2.TabIndex = 1;
            this.button2.Text = "Назад";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.SteelBlue;
            this.label6.Location = new System.Drawing.Point(265, 409);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(221, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Загальна кількість вибраних місць:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.SteelBlue;
            this.label8.Location = new System.Drawing.Point(481, 409);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(14, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.SteelBlue;
            this.label9.Location = new System.Drawing.Point(362, 384);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(94, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "Вибрані місця:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.ForeColor = System.Drawing.Color.SteelBlue;
            this.label10.Location = new System.Drawing.Point(482, 384);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(11, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = " ";
            // 
            // groupBox1
            // 
            this.groupBox1.BackgroundImage = global::client_train.Properties.Resources.gurt;
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.linkLabel59);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.linkLabel57);
            this.groupBox1.Controls.Add(this.linkLabel72);
            this.groupBox1.Controls.Add(this.linkLabel71);
            this.groupBox1.Controls.Add(this.linkLabel70);
            this.groupBox1.Controls.Add(this.linkLabel69);
            this.groupBox1.Controls.Add(this.linkLabel68);
            this.groupBox1.Controls.Add(this.linkLabel67);
            this.groupBox1.Controls.Add(this.linkLabel66);
            this.groupBox1.Controls.Add(this.linkLabel65);
            this.groupBox1.Controls.Add(this.linkLabel64);
            this.groupBox1.Controls.Add(this.linkLabel63);
            this.groupBox1.Controls.Add(this.linkLabel62);
            this.groupBox1.Controls.Add(this.linkLabel61);
            this.groupBox1.Controls.Add(this.linkLabel60);
            this.groupBox1.Controls.Add(this.linkLabel58);
            this.groupBox1.Controls.Add(this.linkLabel56);
            this.groupBox1.Controls.Add(this.linkLabel55);
            this.groupBox1.Controls.Add(this.linkLabel54);
            this.groupBox1.Controls.Add(this.linkLabel53);
            this.groupBox1.Controls.Add(this.linkLabel52);
            this.groupBox1.Controls.Add(this.linkLabel51);
            this.groupBox1.Controls.Add(this.linkLabel50);
            this.groupBox1.Controls.Add(this.linkLabel49);
            this.groupBox1.Controls.Add(this.linkLabel48);
            this.groupBox1.Controls.Add(this.linkLabel47);
            this.groupBox1.Controls.Add(this.linkLabel46);
            this.groupBox1.Controls.Add(this.linkLabel45);
            this.groupBox1.Controls.Add(this.linkLabel44);
            this.groupBox1.Controls.Add(this.linkLabel43);
            this.groupBox1.Controls.Add(this.linkLabel42);
            this.groupBox1.Controls.Add(this.linkLabel41);
            this.groupBox1.Controls.Add(this.linkLabel40);
            this.groupBox1.Controls.Add(this.linkLabel39);
            this.groupBox1.Controls.Add(this.linkLabel38);
            this.groupBox1.Controls.Add(this.linkLabel37);
            this.groupBox1.Controls.Add(this.linkLabel36);
            this.groupBox1.Controls.Add(this.linkLabel35);
            this.groupBox1.Controls.Add(this.linkLabel34);
            this.groupBox1.Controls.Add(this.linkLabel33);
            this.groupBox1.Controls.Add(this.linkLabel32);
            this.groupBox1.Controls.Add(this.linkLabel31);
            this.groupBox1.Controls.Add(this.linkLabel30);
            this.groupBox1.Controls.Add(this.linkLabel29);
            this.groupBox1.Controls.Add(this.linkLabel28);
            this.groupBox1.Controls.Add(this.linkLabel27);
            this.groupBox1.Controls.Add(this.linkLabel26);
            this.groupBox1.Controls.Add(this.linkLabel25);
            this.groupBox1.Controls.Add(this.linkLabel24);
            this.groupBox1.Controls.Add(this.linkLabel23);
            this.groupBox1.Controls.Add(this.linkLabel22);
            this.groupBox1.Controls.Add(this.linkLabel21);
            this.groupBox1.Controls.Add(this.linkLabel20);
            this.groupBox1.Controls.Add(this.linkLabel19);
            this.groupBox1.Controls.Add(this.linkLabel18);
            this.groupBox1.Controls.Add(this.linkLabel17);
            this.groupBox1.Controls.Add(this.linkLabel16);
            this.groupBox1.Controls.Add(this.linkLabel15);
            this.groupBox1.Controls.Add(this.linkLabel14);
            this.groupBox1.Controls.Add(this.linkLabel13);
            this.groupBox1.Controls.Add(this.linkLabel12);
            this.groupBox1.Controls.Add(this.linkLabel11);
            this.groupBox1.Controls.Add(this.linkLabel10);
            this.groupBox1.Controls.Add(this.linkLabel9);
            this.groupBox1.Controls.Add(this.linkLabel8);
            this.groupBox1.Controls.Add(this.linkLabel7);
            this.groupBox1.Controls.Add(this.linkLabel6);
            this.groupBox1.Controls.Add(this.linkLabel5);
            this.groupBox1.Controls.Add(this.linkLabel4);
            this.groupBox1.Controls.Add(this.linkLabel3);
            this.groupBox1.Controls.Add(this.linkLabel2);
            this.groupBox1.Controls.Add(this.linkLabel1);
            this.groupBox1.Location = new System.Drawing.Point(-4, -19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(867, 353);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label4.Location = new System.Drawing.Point(359, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 72;
            this.label4.Text = "label4";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label3.Location = new System.Drawing.Point(312, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 56;
            this.label3.Text = "label3";
            this.label3.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label2.Location = new System.Drawing.Point(174, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 13);
            this.label2.TabIndex = 55;
            this.label2.Text = "label2";
            // 
            // linkLabel59
            // 
            this.linkLabel59.AutoSize = true;
            this.linkLabel59.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel59.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel59.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel59.ForeColor = System.Drawing.Color.Black;
            this.linkLabel59.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel59.Location = new System.Drawing.Point(466, 220);
            this.linkLabel59.Name = "linkLabel59";
            this.linkLabel59.Size = new System.Drawing.Size(26, 17);
            this.linkLabel59.TabIndex = 58;
            this.linkLabel59.TabStop = true;
            this.linkLabel59.Text = "59";
            this.linkLabel59.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel59_LinkClicked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.label1.Location = new System.Drawing.Point(25, 169);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 54;
            this.label1.Text = "label1";
            // 
            // linkLabel57
            // 
            this.linkLabel57.AutoSize = true;
            this.linkLabel57.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel57.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel57.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel57.ForeColor = System.Drawing.Color.Black;
            this.linkLabel57.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel57.Location = new System.Drawing.Point(434, 220);
            this.linkLabel57.Name = "linkLabel57";
            this.linkLabel57.Size = new System.Drawing.Size(26, 17);
            this.linkLabel57.TabIndex = 56;
            this.linkLabel57.TabStop = true;
            this.linkLabel57.Text = "57";
            this.linkLabel57.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel57_LinkClicked);
            // 
            // linkLabel72
            // 
            this.linkLabel72.AutoSize = true;
            this.linkLabel72.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel72.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel72.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel72.ForeColor = System.Drawing.Color.Black;
            this.linkLabel72.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel72.Location = new System.Drawing.Point(669, 247);
            this.linkLabel72.Name = "linkLabel72";
            this.linkLabel72.Size = new System.Drawing.Size(26, 17);
            this.linkLabel72.TabIndex = 71;
            this.linkLabel72.TabStop = true;
            this.linkLabel72.Text = "72";
            this.linkLabel72.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel72_LinkClicked);
            // 
            // linkLabel71
            // 
            this.linkLabel71.AutoSize = true;
            this.linkLabel71.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel71.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel71.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel71.ForeColor = System.Drawing.Color.Black;
            this.linkLabel71.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel71.Location = new System.Drawing.Point(669, 220);
            this.linkLabel71.Name = "linkLabel71";
            this.linkLabel71.Size = new System.Drawing.Size(26, 17);
            this.linkLabel71.TabIndex = 70;
            this.linkLabel71.TabStop = true;
            this.linkLabel71.Text = "71";
            this.linkLabel71.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel71_LinkClicked);
            // 
            // linkLabel70
            // 
            this.linkLabel70.AutoSize = true;
            this.linkLabel70.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel70.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel70.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel70.ForeColor = System.Drawing.Color.Black;
            this.linkLabel70.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel70.Location = new System.Drawing.Point(637, 247);
            this.linkLabel70.Name = "linkLabel70";
            this.linkLabel70.Size = new System.Drawing.Size(26, 17);
            this.linkLabel70.TabIndex = 69;
            this.linkLabel70.TabStop = true;
            this.linkLabel70.Text = "70";
            this.linkLabel70.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel70_LinkClicked);
            // 
            // linkLabel69
            // 
            this.linkLabel69.AutoSize = true;
            this.linkLabel69.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel69.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel69.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel69.ForeColor = System.Drawing.Color.Black;
            this.linkLabel69.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel69.Location = new System.Drawing.Point(637, 220);
            this.linkLabel69.Name = "linkLabel69";
            this.linkLabel69.Size = new System.Drawing.Size(26, 17);
            this.linkLabel69.TabIndex = 68;
            this.linkLabel69.TabStop = true;
            this.linkLabel69.Text = "69";
            this.linkLabel69.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel69_LinkClicked);
            // 
            // linkLabel68
            // 
            this.linkLabel68.AutoSize = true;
            this.linkLabel68.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel68.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel68.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel68.ForeColor = System.Drawing.Color.Black;
            this.linkLabel68.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel68.Location = new System.Drawing.Point(601, 247);
            this.linkLabel68.Name = "linkLabel68";
            this.linkLabel68.Size = new System.Drawing.Size(26, 17);
            this.linkLabel68.TabIndex = 67;
            this.linkLabel68.TabStop = true;
            this.linkLabel68.Text = "68";
            this.linkLabel68.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel68_LinkClicked);
            // 
            // linkLabel67
            // 
            this.linkLabel67.AutoSize = true;
            this.linkLabel67.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel67.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel67.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel67.ForeColor = System.Drawing.Color.Black;
            this.linkLabel67.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel67.Location = new System.Drawing.Point(601, 220);
            this.linkLabel67.Name = "linkLabel67";
            this.linkLabel67.Size = new System.Drawing.Size(26, 17);
            this.linkLabel67.TabIndex = 66;
            this.linkLabel67.TabStop = true;
            this.linkLabel67.Text = "67";
            this.linkLabel67.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel67_LinkClicked);
            // 
            // linkLabel66
            // 
            this.linkLabel66.AutoSize = true;
            this.linkLabel66.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel66.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel66.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel66.ForeColor = System.Drawing.Color.Black;
            this.linkLabel66.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel66.Location = new System.Drawing.Point(569, 247);
            this.linkLabel66.Name = "linkLabel66";
            this.linkLabel66.Size = new System.Drawing.Size(26, 17);
            this.linkLabel66.TabIndex = 65;
            this.linkLabel66.TabStop = true;
            this.linkLabel66.Text = "66";
            this.linkLabel66.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel66_LinkClicked);
            // 
            // linkLabel65
            // 
            this.linkLabel65.AutoSize = true;
            this.linkLabel65.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel65.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel65.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel65.ForeColor = System.Drawing.Color.Black;
            this.linkLabel65.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel65.Location = new System.Drawing.Point(569, 220);
            this.linkLabel65.Name = "linkLabel65";
            this.linkLabel65.Size = new System.Drawing.Size(26, 17);
            this.linkLabel65.TabIndex = 64;
            this.linkLabel65.TabStop = true;
            this.linkLabel65.Text = "65";
            this.linkLabel65.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel65_LinkClicked);
            // 
            // linkLabel64
            // 
            this.linkLabel64.AutoSize = true;
            this.linkLabel64.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel64.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel64.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel64.ForeColor = System.Drawing.Color.Black;
            this.linkLabel64.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel64.Location = new System.Drawing.Point(532, 247);
            this.linkLabel64.Name = "linkLabel64";
            this.linkLabel64.Size = new System.Drawing.Size(26, 17);
            this.linkLabel64.TabIndex = 63;
            this.linkLabel64.TabStop = true;
            this.linkLabel64.Text = "64";
            this.linkLabel64.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel64_LinkClicked);
            // 
            // linkLabel63
            // 
            this.linkLabel63.AutoSize = true;
            this.linkLabel63.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel63.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel63.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel63.ForeColor = System.Drawing.Color.Black;
            this.linkLabel63.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel63.Location = new System.Drawing.Point(530, 220);
            this.linkLabel63.Name = "linkLabel63";
            this.linkLabel63.Size = new System.Drawing.Size(26, 17);
            this.linkLabel63.TabIndex = 62;
            this.linkLabel63.TabStop = true;
            this.linkLabel63.Text = "63";
            this.linkLabel63.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel63_LinkClicked);
            // 
            // linkLabel62
            // 
            this.linkLabel62.AutoSize = true;
            this.linkLabel62.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel62.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel62.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel62.ForeColor = System.Drawing.Color.Black;
            this.linkLabel62.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel62.Location = new System.Drawing.Point(500, 247);
            this.linkLabel62.Name = "linkLabel62";
            this.linkLabel62.Size = new System.Drawing.Size(26, 17);
            this.linkLabel62.TabIndex = 61;
            this.linkLabel62.TabStop = true;
            this.linkLabel62.Text = "62";
            this.linkLabel62.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel62_LinkClicked);
            // 
            // linkLabel61
            // 
            this.linkLabel61.AutoSize = true;
            this.linkLabel61.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel61.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel61.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel61.ForeColor = System.Drawing.Color.Black;
            this.linkLabel61.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel61.Location = new System.Drawing.Point(498, 220);
            this.linkLabel61.Name = "linkLabel61";
            this.linkLabel61.Size = new System.Drawing.Size(26, 17);
            this.linkLabel61.TabIndex = 60;
            this.linkLabel61.TabStop = true;
            this.linkLabel61.Text = "61";
            this.linkLabel61.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel61_LinkClicked);
            // 
            // linkLabel60
            // 
            this.linkLabel60.AutoSize = true;
            this.linkLabel60.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel60.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel60.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel60.ForeColor = System.Drawing.Color.Black;
            this.linkLabel60.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel60.Location = new System.Drawing.Point(465, 247);
            this.linkLabel60.Name = "linkLabel60";
            this.linkLabel60.Size = new System.Drawing.Size(26, 17);
            this.linkLabel60.TabIndex = 59;
            this.linkLabel60.TabStop = true;
            this.linkLabel60.Text = "60";
            this.linkLabel60.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel60_LinkClicked);
            // 
            // linkLabel58
            // 
            this.linkLabel58.AutoSize = true;
            this.linkLabel58.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel58.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel58.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel58.ForeColor = System.Drawing.Color.Black;
            this.linkLabel58.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel58.Location = new System.Drawing.Point(434, 247);
            this.linkLabel58.Name = "linkLabel58";
            this.linkLabel58.Size = new System.Drawing.Size(26, 17);
            this.linkLabel58.TabIndex = 57;
            this.linkLabel58.TabStop = true;
            this.linkLabel58.Text = "58";
            this.linkLabel58.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel58_LinkClicked);
            // 
            // linkLabel56
            // 
            this.linkLabel56.AutoSize = true;
            this.linkLabel56.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel56.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel56.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel56.ForeColor = System.Drawing.Color.Black;
            this.linkLabel56.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel56.Location = new System.Drawing.Point(396, 247);
            this.linkLabel56.Name = "linkLabel56";
            this.linkLabel56.Size = new System.Drawing.Size(26, 17);
            this.linkLabel56.TabIndex = 55;
            this.linkLabel56.TabStop = true;
            this.linkLabel56.Text = "56";
            this.linkLabel56.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel56_LinkClicked);
            // 
            // linkLabel55
            // 
            this.linkLabel55.AutoSize = true;
            this.linkLabel55.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel55.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel55.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel55.ForeColor = System.Drawing.Color.Black;
            this.linkLabel55.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel55.Location = new System.Drawing.Point(396, 220);
            this.linkLabel55.Name = "linkLabel55";
            this.linkLabel55.Size = new System.Drawing.Size(26, 17);
            this.linkLabel55.TabIndex = 54;
            this.linkLabel55.TabStop = true;
            this.linkLabel55.Text = "55";
            this.linkLabel55.VisitedLinkColor = System.Drawing.Color.Black;
            this.linkLabel55.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel55_LinkClicked);
            // 
            // linkLabel54
            // 
            this.linkLabel54.AutoSize = true;
            this.linkLabel54.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel54.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel54.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel54.ForeColor = System.Drawing.Color.Black;
            this.linkLabel54.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel54.Location = new System.Drawing.Point(364, 247);
            this.linkLabel54.Name = "linkLabel54";
            this.linkLabel54.Size = new System.Drawing.Size(26, 17);
            this.linkLabel54.TabIndex = 53;
            this.linkLabel54.TabStop = true;
            this.linkLabel54.Text = "54";
            this.linkLabel54.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel54_LinkClicked);
            // 
            // linkLabel53
            // 
            this.linkLabel53.AutoSize = true;
            this.linkLabel53.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel53.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel53.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel53.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel53.Location = new System.Drawing.Point(364, 220);
            this.linkLabel53.Name = "linkLabel53";
            this.linkLabel53.Size = new System.Drawing.Size(26, 17);
            this.linkLabel53.TabIndex = 52;
            this.linkLabel53.TabStop = true;
            this.linkLabel53.Text = "53";
            this.linkLabel53.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel53_LinkClicked);
            // 
            // linkLabel52
            // 
            this.linkLabel52.AutoSize = true;
            this.linkLabel52.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel52.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel52.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel52.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel52.Location = new System.Drawing.Point(327, 247);
            this.linkLabel52.Name = "linkLabel52";
            this.linkLabel52.Size = new System.Drawing.Size(26, 17);
            this.linkLabel52.TabIndex = 51;
            this.linkLabel52.TabStop = true;
            this.linkLabel52.Text = "52";
            this.linkLabel52.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel52_LinkClicked);
            // 
            // linkLabel51
            // 
            this.linkLabel51.AutoSize = true;
            this.linkLabel51.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel51.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel51.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel51.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel51.Location = new System.Drawing.Point(327, 220);
            this.linkLabel51.Name = "linkLabel51";
            this.linkLabel51.Size = new System.Drawing.Size(26, 17);
            this.linkLabel51.TabIndex = 50;
            this.linkLabel51.TabStop = true;
            this.linkLabel51.Text = "51";
            this.linkLabel51.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel51_LinkClicked);
            // 
            // linkLabel50
            // 
            this.linkLabel50.AutoSize = true;
            this.linkLabel50.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel50.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel50.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel50.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel50.Location = new System.Drawing.Point(295, 247);
            this.linkLabel50.Name = "linkLabel50";
            this.linkLabel50.Size = new System.Drawing.Size(26, 17);
            this.linkLabel50.TabIndex = 49;
            this.linkLabel50.TabStop = true;
            this.linkLabel50.Text = "50";
            this.linkLabel50.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel50_LinkClicked);
            // 
            // linkLabel49
            // 
            this.linkLabel49.AutoSize = true;
            this.linkLabel49.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel49.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel49.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel49.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel49.Location = new System.Drawing.Point(295, 220);
            this.linkLabel49.Name = "linkLabel49";
            this.linkLabel49.Size = new System.Drawing.Size(26, 17);
            this.linkLabel49.TabIndex = 48;
            this.linkLabel49.TabStop = true;
            this.linkLabel49.Text = "49";
            this.linkLabel49.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel49_LinkClicked);
            // 
            // linkLabel48
            // 
            this.linkLabel48.AutoSize = true;
            this.linkLabel48.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel48.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel48.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel48.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel48.Location = new System.Drawing.Point(260, 247);
            this.linkLabel48.Name = "linkLabel48";
            this.linkLabel48.Size = new System.Drawing.Size(26, 17);
            this.linkLabel48.TabIndex = 47;
            this.linkLabel48.TabStop = true;
            this.linkLabel48.Text = "48";
            this.linkLabel48.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel48_LinkClicked);
            // 
            // linkLabel47
            // 
            this.linkLabel47.AutoSize = true;
            this.linkLabel47.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel47.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel47.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel47.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel47.Location = new System.Drawing.Point(260, 220);
            this.linkLabel47.Name = "linkLabel47";
            this.linkLabel47.Size = new System.Drawing.Size(26, 17);
            this.linkLabel47.TabIndex = 46;
            this.linkLabel47.TabStop = true;
            this.linkLabel47.Text = "47";
            this.linkLabel47.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel47_LinkClicked);
            // 
            // linkLabel46
            // 
            this.linkLabel46.AutoSize = true;
            this.linkLabel46.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel46.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel46.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel46.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel46.Location = new System.Drawing.Point(228, 247);
            this.linkLabel46.Name = "linkLabel46";
            this.linkLabel46.Size = new System.Drawing.Size(26, 17);
            this.linkLabel46.TabIndex = 45;
            this.linkLabel46.TabStop = true;
            this.linkLabel46.Text = "46";
            this.linkLabel46.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel46_LinkClicked);
            // 
            // linkLabel45
            // 
            this.linkLabel45.AutoSize = true;
            this.linkLabel45.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel45.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel45.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel45.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel45.Location = new System.Drawing.Point(228, 220);
            this.linkLabel45.Name = "linkLabel45";
            this.linkLabel45.Size = new System.Drawing.Size(26, 17);
            this.linkLabel45.TabIndex = 44;
            this.linkLabel45.TabStop = true;
            this.linkLabel45.Text = "45";
            this.linkLabel45.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel45_LinkClicked);
            // 
            // linkLabel44
            // 
            this.linkLabel44.AutoSize = true;
            this.linkLabel44.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel44.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel44.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel44.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel44.Location = new System.Drawing.Point(194, 247);
            this.linkLabel44.Name = "linkLabel44";
            this.linkLabel44.Size = new System.Drawing.Size(26, 17);
            this.linkLabel44.TabIndex = 43;
            this.linkLabel44.TabStop = true;
            this.linkLabel44.Text = "44";
            this.linkLabel44.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel44_LinkClicked);
            // 
            // linkLabel43
            // 
            this.linkLabel43.AutoSize = true;
            this.linkLabel43.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel43.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel43.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel43.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel43.Location = new System.Drawing.Point(192, 220);
            this.linkLabel43.Name = "linkLabel43";
            this.linkLabel43.Size = new System.Drawing.Size(26, 17);
            this.linkLabel43.TabIndex = 42;
            this.linkLabel43.TabStop = true;
            this.linkLabel43.Text = "43";
            this.linkLabel43.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel43_LinkClicked);
            // 
            // linkLabel42
            // 
            this.linkLabel42.AutoSize = true;
            this.linkLabel42.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel42.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel42.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel42.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel42.Location = new System.Drawing.Point(162, 247);
            this.linkLabel42.Name = "linkLabel42";
            this.linkLabel42.Size = new System.Drawing.Size(26, 17);
            this.linkLabel42.TabIndex = 41;
            this.linkLabel42.TabStop = true;
            this.linkLabel42.Text = "42";
            this.linkLabel42.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel42_LinkClicked);
            // 
            // linkLabel41
            // 
            this.linkLabel41.AutoSize = true;
            this.linkLabel41.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel41.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel41.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel41.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel41.Location = new System.Drawing.Point(163, 220);
            this.linkLabel41.Name = "linkLabel41";
            this.linkLabel41.Size = new System.Drawing.Size(26, 17);
            this.linkLabel41.TabIndex = 40;
            this.linkLabel41.TabStop = true;
            this.linkLabel41.Text = "41";
            this.linkLabel41.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel41_LinkClicked);
            // 
            // linkLabel40
            // 
            this.linkLabel40.AutoSize = true;
            this.linkLabel40.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel40.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel40.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel40.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel40.Location = new System.Drawing.Point(125, 247);
            this.linkLabel40.Name = "linkLabel40";
            this.linkLabel40.Size = new System.Drawing.Size(26, 17);
            this.linkLabel40.TabIndex = 39;
            this.linkLabel40.TabStop = true;
            this.linkLabel40.Text = "40";
            this.linkLabel40.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel40_LinkClicked);
            // 
            // linkLabel39
            // 
            this.linkLabel39.AutoSize = true;
            this.linkLabel39.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel39.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel39.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel39.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel39.Location = new System.Drawing.Point(125, 220);
            this.linkLabel39.Name = "linkLabel39";
            this.linkLabel39.Size = new System.Drawing.Size(26, 17);
            this.linkLabel39.TabIndex = 38;
            this.linkLabel39.TabStop = true;
            this.linkLabel39.Text = "39";
            this.linkLabel39.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel39_LinkClicked);
            // 
            // linkLabel38
            // 
            this.linkLabel38.AutoSize = true;
            this.linkLabel38.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel38.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel38.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel38.Location = new System.Drawing.Point(93, 247);
            this.linkLabel38.Name = "linkLabel38";
            this.linkLabel38.Size = new System.Drawing.Size(26, 17);
            this.linkLabel38.TabIndex = 37;
            this.linkLabel38.TabStop = true;
            this.linkLabel38.Text = "38";
            this.linkLabel38.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel38_LinkClicked);
            // 
            // linkLabel37
            // 
            this.linkLabel37.AutoSize = true;
            this.linkLabel37.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel37.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel37.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel37.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel37.Location = new System.Drawing.Point(93, 220);
            this.linkLabel37.Name = "linkLabel37";
            this.linkLabel37.Size = new System.Drawing.Size(26, 17);
            this.linkLabel37.TabIndex = 36;
            this.linkLabel37.TabStop = true;
            this.linkLabel37.Text = "37";
            this.linkLabel37.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel37_LinkClicked);
            // 
            // linkLabel36
            // 
            this.linkLabel36.AutoSize = true;
            this.linkLabel36.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel36.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel36.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel36.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel36.Location = new System.Drawing.Point(735, 115);
            this.linkLabel36.Name = "linkLabel36";
            this.linkLabel36.Size = new System.Drawing.Size(26, 17);
            this.linkLabel36.TabIndex = 35;
            this.linkLabel36.TabStop = true;
            this.linkLabel36.Text = "36";
            this.linkLabel36.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel36_LinkClicked);
            // 
            // linkLabel35
            // 
            this.linkLabel35.AutoSize = true;
            this.linkLabel35.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel35.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel35.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel35.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel35.Location = new System.Drawing.Point(735, 150);
            this.linkLabel35.Name = "linkLabel35";
            this.linkLabel35.Size = new System.Drawing.Size(26, 17);
            this.linkLabel35.TabIndex = 34;
            this.linkLabel35.TabStop = true;
            this.linkLabel35.Text = "35";
            this.linkLabel35.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel35_LinkClicked);
            // 
            // linkLabel34
            // 
            this.linkLabel34.AutoSize = true;
            this.linkLabel34.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel34.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel34.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel34.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel34.Location = new System.Drawing.Point(703, 115);
            this.linkLabel34.Name = "linkLabel34";
            this.linkLabel34.Size = new System.Drawing.Size(26, 17);
            this.linkLabel34.TabIndex = 33;
            this.linkLabel34.TabStop = true;
            this.linkLabel34.Text = "34";
            this.linkLabel34.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel34_LinkClicked);
            // 
            // linkLabel33
            // 
            this.linkLabel33.AutoSize = true;
            this.linkLabel33.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel33.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel33.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel33.Location = new System.Drawing.Point(703, 150);
            this.linkLabel33.Name = "linkLabel33";
            this.linkLabel33.Size = new System.Drawing.Size(26, 17);
            this.linkLabel33.TabIndex = 32;
            this.linkLabel33.TabStop = true;
            this.linkLabel33.Text = "33";
            this.linkLabel33.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel33_LinkClicked);
            // 
            // linkLabel32
            // 
            this.linkLabel32.AutoSize = true;
            this.linkLabel32.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel32.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel32.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel32.Location = new System.Drawing.Point(667, 115);
            this.linkLabel32.Name = "linkLabel32";
            this.linkLabel32.Size = new System.Drawing.Size(26, 17);
            this.linkLabel32.TabIndex = 31;
            this.linkLabel32.TabStop = true;
            this.linkLabel32.Text = "32";
            this.linkLabel32.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel32_LinkClicked);
            // 
            // linkLabel31
            // 
            this.linkLabel31.AutoSize = true;
            this.linkLabel31.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel31.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel31.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel31.Location = new System.Drawing.Point(667, 150);
            this.linkLabel31.Name = "linkLabel31";
            this.linkLabel31.Size = new System.Drawing.Size(26, 17);
            this.linkLabel31.TabIndex = 30;
            this.linkLabel31.TabStop = true;
            this.linkLabel31.Text = "31";
            this.linkLabel31.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel31_LinkClicked);
            // 
            // linkLabel30
            // 
            this.linkLabel30.AutoSize = true;
            this.linkLabel30.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel30.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel30.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel30.Location = new System.Drawing.Point(637, 115);
            this.linkLabel30.Name = "linkLabel30";
            this.linkLabel30.Size = new System.Drawing.Size(26, 17);
            this.linkLabel30.TabIndex = 29;
            this.linkLabel30.TabStop = true;
            this.linkLabel30.Text = "30";
            this.linkLabel30.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel30_LinkClicked);
            // 
            // linkLabel29
            // 
            this.linkLabel29.AutoSize = true;
            this.linkLabel29.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel29.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel29.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel29.Location = new System.Drawing.Point(637, 150);
            this.linkLabel29.Name = "linkLabel29";
            this.linkLabel29.Size = new System.Drawing.Size(26, 17);
            this.linkLabel29.TabIndex = 28;
            this.linkLabel29.TabStop = true;
            this.linkLabel29.Text = "29";
            this.linkLabel29.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel29_LinkClicked);
            // 
            // linkLabel28
            // 
            this.linkLabel28.AutoSize = true;
            this.linkLabel28.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel28.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel28.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel28.Location = new System.Drawing.Point(599, 115);
            this.linkLabel28.Name = "linkLabel28";
            this.linkLabel28.Size = new System.Drawing.Size(26, 17);
            this.linkLabel28.TabIndex = 27;
            this.linkLabel28.TabStop = true;
            this.linkLabel28.Text = "28";
            this.linkLabel28.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel28_LinkClicked);
            // 
            // linkLabel27
            // 
            this.linkLabel27.AutoSize = true;
            this.linkLabel27.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel27.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel27.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel27.Location = new System.Drawing.Point(599, 150);
            this.linkLabel27.Name = "linkLabel27";
            this.linkLabel27.Size = new System.Drawing.Size(26, 17);
            this.linkLabel27.TabIndex = 26;
            this.linkLabel27.TabStop = true;
            this.linkLabel27.Text = "27";
            this.linkLabel27.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel27_LinkClicked);
            // 
            // linkLabel26
            // 
            this.linkLabel26.AutoSize = true;
            this.linkLabel26.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel26.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel26.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel26.Location = new System.Drawing.Point(569, 115);
            this.linkLabel26.Name = "linkLabel26";
            this.linkLabel26.Size = new System.Drawing.Size(26, 17);
            this.linkLabel26.TabIndex = 25;
            this.linkLabel26.TabStop = true;
            this.linkLabel26.Text = "26";
            this.linkLabel26.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel26_LinkClicked);
            // 
            // linkLabel25
            // 
            this.linkLabel25.AutoSize = true;
            this.linkLabel25.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel25.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel25.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel25.Location = new System.Drawing.Point(569, 150);
            this.linkLabel25.Name = "linkLabel25";
            this.linkLabel25.Size = new System.Drawing.Size(26, 17);
            this.linkLabel25.TabIndex = 24;
            this.linkLabel25.TabStop = true;
            this.linkLabel25.Text = "25";
            this.linkLabel25.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel25_LinkClicked);
            // 
            // linkLabel24
            // 
            this.linkLabel24.AutoSize = true;
            this.linkLabel24.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel24.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel24.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel24.Location = new System.Drawing.Point(532, 115);
            this.linkLabel24.Name = "linkLabel24";
            this.linkLabel24.Size = new System.Drawing.Size(26, 17);
            this.linkLabel24.TabIndex = 23;
            this.linkLabel24.TabStop = true;
            this.linkLabel24.Text = "24";
            this.linkLabel24.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel24_LinkClicked);
            // 
            // linkLabel23
            // 
            this.linkLabel23.AutoSize = true;
            this.linkLabel23.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel23.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel23.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel23.Location = new System.Drawing.Point(532, 150);
            this.linkLabel23.Name = "linkLabel23";
            this.linkLabel23.Size = new System.Drawing.Size(26, 17);
            this.linkLabel23.TabIndex = 22;
            this.linkLabel23.TabStop = true;
            this.linkLabel23.Text = "23";
            this.linkLabel23.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel23_LinkClicked);
            // 
            // linkLabel22
            // 
            this.linkLabel22.AutoSize = true;
            this.linkLabel22.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel22.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel22.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel22.Location = new System.Drawing.Point(500, 115);
            this.linkLabel22.Name = "linkLabel22";
            this.linkLabel22.Size = new System.Drawing.Size(26, 17);
            this.linkLabel22.TabIndex = 21;
            this.linkLabel22.TabStop = true;
            this.linkLabel22.Text = "22";
            this.linkLabel22.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel22_LinkClicked);
            // 
            // linkLabel21
            // 
            this.linkLabel21.AutoSize = true;
            this.linkLabel21.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel21.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel21.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel21.Location = new System.Drawing.Point(500, 150);
            this.linkLabel21.Name = "linkLabel21";
            this.linkLabel21.Size = new System.Drawing.Size(26, 17);
            this.linkLabel21.TabIndex = 20;
            this.linkLabel21.TabStop = true;
            this.linkLabel21.Text = "21";
            this.linkLabel21.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel21_LinkClicked);
            // 
            // linkLabel20
            // 
            this.linkLabel20.AutoSize = true;
            this.linkLabel20.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel20.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel20.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel20.Location = new System.Drawing.Point(465, 115);
            this.linkLabel20.Name = "linkLabel20";
            this.linkLabel20.Size = new System.Drawing.Size(26, 17);
            this.linkLabel20.TabIndex = 19;
            this.linkLabel20.TabStop = true;
            this.linkLabel20.Text = "20";
            this.linkLabel20.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel20_LinkClicked);
            // 
            // linkLabel19
            // 
            this.linkLabel19.AutoSize = true;
            this.linkLabel19.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel19.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel19.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel19.Location = new System.Drawing.Point(465, 150);
            this.linkLabel19.Name = "linkLabel19";
            this.linkLabel19.Size = new System.Drawing.Size(26, 17);
            this.linkLabel19.TabIndex = 18;
            this.linkLabel19.TabStop = true;
            this.linkLabel19.Text = "19";
            this.linkLabel19.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel19_LinkClicked);
            // 
            // linkLabel18
            // 
            this.linkLabel18.AutoSize = true;
            this.linkLabel18.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel18.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel18.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel18.Location = new System.Drawing.Point(432, 115);
            this.linkLabel18.Name = "linkLabel18";
            this.linkLabel18.Size = new System.Drawing.Size(26, 17);
            this.linkLabel18.TabIndex = 17;
            this.linkLabel18.TabStop = true;
            this.linkLabel18.Text = "18";
            this.linkLabel18.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel18_LinkClicked);
            // 
            // linkLabel17
            // 
            this.linkLabel17.AutoSize = true;
            this.linkLabel17.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel17.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel17.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel17.Location = new System.Drawing.Point(432, 150);
            this.linkLabel17.Name = "linkLabel17";
            this.linkLabel17.Size = new System.Drawing.Size(26, 17);
            this.linkLabel17.TabIndex = 16;
            this.linkLabel17.TabStop = true;
            this.linkLabel17.Text = "17";
            this.linkLabel17.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel17_LinkClicked);
            // 
            // linkLabel16
            // 
            this.linkLabel16.AutoSize = true;
            this.linkLabel16.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel16.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel16.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel16.Location = new System.Drawing.Point(396, 115);
            this.linkLabel16.Name = "linkLabel16";
            this.linkLabel16.Size = new System.Drawing.Size(26, 17);
            this.linkLabel16.TabIndex = 15;
            this.linkLabel16.TabStop = true;
            this.linkLabel16.Text = "16";
            this.linkLabel16.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel16_LinkClicked);
            // 
            // linkLabel15
            // 
            this.linkLabel15.AutoSize = true;
            this.linkLabel15.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel15.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel15.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel15.Location = new System.Drawing.Point(396, 150);
            this.linkLabel15.Name = "linkLabel15";
            this.linkLabel15.Size = new System.Drawing.Size(26, 17);
            this.linkLabel15.TabIndex = 14;
            this.linkLabel15.TabStop = true;
            this.linkLabel15.Text = "15";
            this.linkLabel15.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel15_LinkClicked);
            // 
            // linkLabel14
            // 
            this.linkLabel14.AutoSize = true;
            this.linkLabel14.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel14.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel14.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel14.Location = new System.Drawing.Point(364, 115);
            this.linkLabel14.Name = "linkLabel14";
            this.linkLabel14.Size = new System.Drawing.Size(26, 17);
            this.linkLabel14.TabIndex = 13;
            this.linkLabel14.TabStop = true;
            this.linkLabel14.Text = "14";
            this.linkLabel14.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel14_LinkClicked);
            // 
            // linkLabel13
            // 
            this.linkLabel13.AutoSize = true;
            this.linkLabel13.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel13.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel13.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel13.Location = new System.Drawing.Point(364, 150);
            this.linkLabel13.Name = "linkLabel13";
            this.linkLabel13.Size = new System.Drawing.Size(26, 17);
            this.linkLabel13.TabIndex = 12;
            this.linkLabel13.TabStop = true;
            this.linkLabel13.Text = "13";
            this.linkLabel13.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel13_LinkClicked);
            // 
            // linkLabel12
            // 
            this.linkLabel12.AutoSize = true;
            this.linkLabel12.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel12.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel12.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel12.Location = new System.Drawing.Point(327, 115);
            this.linkLabel12.Name = "linkLabel12";
            this.linkLabel12.Size = new System.Drawing.Size(26, 17);
            this.linkLabel12.TabIndex = 11;
            this.linkLabel12.TabStop = true;
            this.linkLabel12.Text = "12";
            this.linkLabel12.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel12_LinkClicked);
            // 
            // linkLabel11
            // 
            this.linkLabel11.AutoSize = true;
            this.linkLabel11.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel11.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel11.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel11.Location = new System.Drawing.Point(327, 150);
            this.linkLabel11.Name = "linkLabel11";
            this.linkLabel11.Size = new System.Drawing.Size(26, 17);
            this.linkLabel11.TabIndex = 10;
            this.linkLabel11.TabStop = true;
            this.linkLabel11.Text = "11";
            this.linkLabel11.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel11_LinkClicked);
            // 
            // linkLabel10
            // 
            this.linkLabel10.AutoSize = true;
            this.linkLabel10.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel10.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel10.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel10.Location = new System.Drawing.Point(295, 115);
            this.linkLabel10.Name = "linkLabel10";
            this.linkLabel10.Size = new System.Drawing.Size(26, 17);
            this.linkLabel10.TabIndex = 9;
            this.linkLabel10.TabStop = true;
            this.linkLabel10.Text = "10";
            this.linkLabel10.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel10_LinkClicked);
            // 
            // linkLabel9
            // 
            this.linkLabel9.AutoSize = true;
            this.linkLabel9.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel9.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel9.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel9.Location = new System.Drawing.Point(301, 150);
            this.linkLabel9.Name = "linkLabel9";
            this.linkLabel9.Size = new System.Drawing.Size(17, 17);
            this.linkLabel9.TabIndex = 8;
            this.linkLabel9.TabStop = true;
            this.linkLabel9.Text = "9";
            this.linkLabel9.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel9_LinkClicked);
            // 
            // linkLabel8
            // 
            this.linkLabel8.AutoSize = true;
            this.linkLabel8.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel8.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel8.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel8.Location = new System.Drawing.Point(265, 115);
            this.linkLabel8.Name = "linkLabel8";
            this.linkLabel8.Size = new System.Drawing.Size(17, 17);
            this.linkLabel8.TabIndex = 7;
            this.linkLabel8.TabStop = true;
            this.linkLabel8.Text = "8";
            this.linkLabel8.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel8_LinkClicked);
            // 
            // linkLabel7
            // 
            this.linkLabel7.AutoSize = true;
            this.linkLabel7.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel7.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel7.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel7.Location = new System.Drawing.Point(265, 150);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(17, 17);
            this.linkLabel7.TabIndex = 6;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "7";
            this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel7_LinkClicked);
            // 
            // linkLabel6
            // 
            this.linkLabel6.AutoSize = true;
            this.linkLabel6.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel6.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel6.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel6.Location = new System.Drawing.Point(233, 115);
            this.linkLabel6.Name = "linkLabel6";
            this.linkLabel6.Size = new System.Drawing.Size(17, 17);
            this.linkLabel6.TabIndex = 5;
            this.linkLabel6.TabStop = true;
            this.linkLabel6.Text = "6";
            this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
            // 
            // linkLabel5
            // 
            this.linkLabel5.AutoSize = true;
            this.linkLabel5.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel5.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel5.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel5.Location = new System.Drawing.Point(233, 150);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(17, 17);
            this.linkLabel5.TabIndex = 4;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "5";
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
            // 
            // linkLabel4
            // 
            this.linkLabel4.AutoSize = true;
            this.linkLabel4.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel4.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel4.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel4.Location = new System.Drawing.Point(198, 115);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(17, 17);
            this.linkLabel4.TabIndex = 3;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "4";
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.AutoSize = true;
            this.linkLabel3.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel3.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel3.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel3.Location = new System.Drawing.Point(197, 150);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(17, 17);
            this.linkLabel3.TabIndex = 2;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "3";
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel2.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel2.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel2.Location = new System.Drawing.Point(163, 115);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(17, 17);
            this.linkLabel2.TabIndex = 1;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "2";
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.DisabledLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.linkLabel1.ForeColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel1.LinkColor = System.Drawing.Color.MidnightBlue;
            this.linkLabel1.Location = new System.Drawing.Point(163, 150);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(17, 17);
            this.linkLabel1.TabIndex = 0;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "1";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // Form4
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(863, 501);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.ForeColor = System.Drawing.Color.MidnightBlue;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form4";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form4";
            this.Load += new System.EventHandler(this.Form4_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.LinkLabel linkLabel32;
        private System.Windows.Forms.LinkLabel linkLabel31;
        private System.Windows.Forms.LinkLabel linkLabel30;
        private System.Windows.Forms.LinkLabel linkLabel29;
        private System.Windows.Forms.LinkLabel linkLabel28;
        private System.Windows.Forms.LinkLabel linkLabel27;
        private System.Windows.Forms.LinkLabel linkLabel26;
        private System.Windows.Forms.LinkLabel linkLabel25;
        private System.Windows.Forms.LinkLabel linkLabel24;
        private System.Windows.Forms.LinkLabel linkLabel23;
        private System.Windows.Forms.LinkLabel linkLabel22;
        private System.Windows.Forms.LinkLabel linkLabel21;
        private System.Windows.Forms.LinkLabel linkLabel20;
        private System.Windows.Forms.LinkLabel linkLabel19;
        private System.Windows.Forms.LinkLabel linkLabel18;
        private System.Windows.Forms.LinkLabel linkLabel17;
        private System.Windows.Forms.LinkLabel linkLabel16;
        private System.Windows.Forms.LinkLabel linkLabel15;
        private System.Windows.Forms.LinkLabel linkLabel14;
        private System.Windows.Forms.LinkLabel linkLabel13;
        private System.Windows.Forms.LinkLabel linkLabel12;
        private System.Windows.Forms.LinkLabel linkLabel11;
        private System.Windows.Forms.LinkLabel linkLabel10;
        private System.Windows.Forms.LinkLabel linkLabel9;
        private System.Windows.Forms.LinkLabel linkLabel8;
        private System.Windows.Forms.LinkLabel linkLabel7;
        private System.Windows.Forms.LinkLabel linkLabel6;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.LinkLabel linkLabel54;
        private System.Windows.Forms.LinkLabel linkLabel53;
        private System.Windows.Forms.LinkLabel linkLabel52;
        private System.Windows.Forms.LinkLabel linkLabel51;
        private System.Windows.Forms.LinkLabel linkLabel50;
        private System.Windows.Forms.LinkLabel linkLabel49;
        private System.Windows.Forms.LinkLabel linkLabel48;
        private System.Windows.Forms.LinkLabel linkLabel47;
        private System.Windows.Forms.LinkLabel linkLabel46;
        private System.Windows.Forms.LinkLabel linkLabel45;
        private System.Windows.Forms.LinkLabel linkLabel44;
        private System.Windows.Forms.LinkLabel linkLabel43;
        private System.Windows.Forms.LinkLabel linkLabel42;
        private System.Windows.Forms.LinkLabel linkLabel41;
        private System.Windows.Forms.LinkLabel linkLabel40;
        private System.Windows.Forms.LinkLabel linkLabel39;
        private System.Windows.Forms.LinkLabel linkLabel38;
        private System.Windows.Forms.LinkLabel linkLabel37;
        private System.Windows.Forms.LinkLabel linkLabel36;
        private System.Windows.Forms.LinkLabel linkLabel35;
        private System.Windows.Forms.LinkLabel linkLabel34;
        private System.Windows.Forms.LinkLabel linkLabel33;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel linkLabel59;
        private System.Windows.Forms.LinkLabel linkLabel57;
        private System.Windows.Forms.LinkLabel linkLabel72;
        private System.Windows.Forms.LinkLabel linkLabel71;
        private System.Windows.Forms.LinkLabel linkLabel70;
        private System.Windows.Forms.LinkLabel linkLabel69;
        private System.Windows.Forms.LinkLabel linkLabel68;
        private System.Windows.Forms.LinkLabel linkLabel67;
        private System.Windows.Forms.LinkLabel linkLabel66;
        private System.Windows.Forms.LinkLabel linkLabel65;
        private System.Windows.Forms.LinkLabel linkLabel64;
        private System.Windows.Forms.LinkLabel linkLabel63;
        private System.Windows.Forms.LinkLabel linkLabel62;
        private System.Windows.Forms.LinkLabel linkLabel61;
        private System.Windows.Forms.LinkLabel linkLabel60;
        private System.Windows.Forms.LinkLabel linkLabel58;
        private System.Windows.Forms.LinkLabel linkLabel56;
        private System.Windows.Forms.LinkLabel linkLabel55;
    }
}