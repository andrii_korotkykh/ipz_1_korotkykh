﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace client_train
{
    public partial class Form2 : Form
    {

        Form1 f1 = new Form1();
        public Form2(Form1 f1)
        {
           // f1.Hide();
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //f1.Show();
            if (textBox2.Text != textBox3.Text)
                MessageBox.Show("Пароль не збігається");
            else
                if (textBox1.Text == "" || textBox4.Text == "" || textBox5.Text == "")
                    MessageBox.Show("Не заповнені обов'язкові поля");
                else {
                    string message = textBox1.Text + "#" + textBox2.Text + "#" + textBox4.Text + "#" + textBox5.Text + "#" + textBox6.Text + "#" + textBox7.Text + "#" + textBox8.Text + "#"; //login + password + name + secondname + phone + email + worker_id
                    Connect("192.168.0.106", message);

                  //  MessageBox.Show("Реєстрація успішна");
                  
                }

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        public void Connect(String server, String message)
        {
            // index = 1  -   надсилання запита для перевірки аунтифікації
            // index = 2  -   надсилання запита для реєстрації користувача
            // index = 3  -   надсилання запита для пошуку маршрута
            // index = 4  -   надсилання запита для оформлення замовлення


            int index = 2;

            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////
            //
            //index = 2  -   надсилання запита для реєстрації користувача
            //
            ////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////////////////////////////////////////////
               try{
                    // створюєм TcpClient.
                    // Для е TcpListener 
                    // настроюєм на IP нашего сервера и той самий порт.
                    String responseData = String.Empty;
                    Int32 port = 58190;
                    System.Net.Sockets.TcpClient client = new System.Net.Sockets.TcpClient(server, port);
                    // Переводимо наше повідомлення в ЮТФ8, а потім в масив Byte.
                    Byte[] data = System.Text.Encoding.UTF8.GetBytes(index.ToString() + message);
                    // Получаем поток для чтения и записи данных.
                    NetworkStream stream = client.GetStream();
                    //відправяєм повідомлення  серверу.
                    stream.Write(data, 0, data.Length);
                    data = new Byte[512];  // Строка для хранения полученных данных.
                    // Читаем перший пакет відповіді сервера. 
                    // можна читати всу повідомнення
                    // дя цього треба зробити цикл як на сервері     

                    int krt = stream.Read(data, 0, data.Length);
                    responseData = System.Text.Encoding.UTF8.GetString(data, 0, krt);

                    String cod = String.Empty;
                    cod += responseData[0];
                    //Convert.ToInt32(cod) == 9 помилка
                    //Convert.ToInt32(cod) == 1 приймання 1 рядка даних
                   
                    if (Convert.ToInt32(cod) != 9)
                    {                                          
                       MessageBox.Show("Реєстрація завершена успішно:) ");
                       this.Close();
                    }
                    else
                        MessageBox.Show(" Даний логін уже зайнятий :( ");  
                    stream.Close();
                    client.Close();

                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine("ArgumentNullException: {0}", e);
                    MessageBox.Show("Немає звязку з сервером");
                }
                catch (SocketException e)
                {
                    Console.WriteLine("SocketException: {0}", e);
                    MessageBox.Show("Немає звязку з сервером");
                }            

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}
